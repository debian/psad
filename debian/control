Source: psad
Section: admin
Priority: optional
Maintainer: Thiago Andrade Marques <andrade@debian.org>
Build-Depends: debhelper-compat (= 13)
Standards-Version: 4.6.2
Rules-Requires-Root: no
Vcs-Git: https://salsa.debian.org/debian/psad.git
Vcs-Browser: https://salsa.debian.org/debian/psad
Homepage: https://www.cipherdyne.org/psad/

Package: psad
Architecture: any
Pre-Depends: ${misc:Pre-Depends}
Depends: ${misc:Depends}, ${shlibs:Depends}, ${perl:Depends},
 libunix-syslog-perl, iptables, rsyslog | system-log-daemon,
 libnet-ip-perl, libdate-calc-perl, libcarp-clan-perl,
 whois, psmisc, libiptables-parse-perl, libiptables-chainmgr-perl,
 default-mta | mail-transport-agent, bsd-mailx | mailx | mailutils,
 lsb-base, net-tools, iproute2
Suggests: fwsnort
Description: Port Scan Attack Detector
 PSAD is a collection of four lightweight system daemons (in Perl and
 C) designed to work with iptables to detect port scans. It features:
  * a set of highly configurable danger thresholds (with sensible
    defaults provided);
  * verbose alert messages that include the source, destination,
    scanned port range, beginning and end times, TCP flags, and
    corresponding Nmap options;
  * reverse DNS information;
  * alerts via email;
  * automatic blocking of offending IP addresses via dynamic firewall
    configuration.
 .
 When combined with fwsnort and the iptables string match extension,
 PSAD is capable of detecting many attacks described in the Snort rule
 set that involve application layer data.
